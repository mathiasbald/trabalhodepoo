/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import disciplina.Aula;
import disciplina.DiaDaSemana;
import disciplina.Disciplina;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mathias
 */
public class GerenciadorDisciplina {

    private static final GerenciadorDisciplina ME = new GerenciadorDisciplina();

    public static GerenciadorDisciplina getInstance() {
        return ME;
    }
    private String caminho = "src/Arquivos/aulas.bin";
    List<Disciplina> disciplinas;

    private GerenciadorDisciplina() {
        this.disciplinas = ler();
    }

    /**
     * Método que adiciona uma disciplina
     *
     * @param d recebida para ser adicionada
     * @return True caso foi adicionada. Caso contrário, retorna False.
     */
    public boolean addDisciplina(Disciplina d) {
        if (d == null) {
            return false;
        }
        return disciplinas.add(d);
    }

    /**
     * Método que salva as disciplinas em um arquivo binário
     *
     * @throws FileNotFoundException caso não encontre o arquivo
     * @throws IOException exceções do tipo de entrada e saída
     */
    public void salvar() throws FileNotFoundException, IOException {
        File f = new File(caminho);
        f.mkdirs();
        if (!f.exists()) {
            f.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(disciplinas);
        oos.close();
        fos.close();

    }

    /**
     * Método que faz a leitura do arquivo bin e insere na lista
     *
     * @return List com todos os objetos contidos no arquivo
     */
    public List<Disciplina> ler() {
        File f = new File(caminho);
        if (f.exists()) {
            try {
                FileInputStream fis = new FileInputStream(f);
                ObjectInputStream ois = new ObjectInputStream(fis);
                List<Disciplina> lista = (List<Disciplina>) ois.readObject();
                ois.close();
                fis.close();
                return lista;
            } catch (IOException ex) {
                Logger.getLogger(GerenciadorDisciplina.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(GerenciadorDisciplina.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * Método que retorna todas as aulas contida em um dia da semana
     *
     * @param dia dia da semana para pegar as aulas contidas nela
     * @return List com todas as aulas contidas no dia passado
     */
    public List<Disciplina> getDisciplinasDoDia(DiaDaSemana dia) {
        ArrayList<Disciplina> listaParaRetorno = new ArrayList<>();
        for (Disciplina disciplina : disciplinas) {
            if (disciplina.contaisAulasNoDia(dia)) {
                listaParaRetorno.add(disciplina);
            }
        }
        return listaParaRetorno;
    }

    /**
     * Método que deleta a aula apenas de um dia específico
     *
     * @param disciplina recebido para identificar qual disciplina
     * @param dia recebido para deletar a aula apenas daquele dia
     * @return True caso foi excluida com sucesso. Caso contrário, retorna False
     */
    public boolean deleteAulaDoDia(Disciplina disciplina, DiaDaSemana dia) {
        List<Aula> listaPega;
        for (int i = 0; i < disciplinas.size(); i++) {
            if (disciplinas.get(i).equals(disciplina)) {
                listaPega = disciplinas.get(i).getTodasAulas();
                for (Aula aula : listaPega) {
                    if (aula.equalsDiaDaSemana(dia)) {
                        return listaPega.remove(aula);
                    }
                }
            }
        }
        return false;
    }

    /**
     * Método que edita uma disciplina existente
     *
     * @param disciplinaVelha disciplina a ser editada
     * @param novaDisciplina nova disciplina editada
     */
    public void replaceDisciplina(Disciplina disciplinaVelha, Disciplina novaDisciplina) {
        for (int i = 0; i < disciplinas.size(); i++) {
            if (disciplinas.get(i).equals(disciplinaVelha)) {
                disciplinas.set(i, novaDisciplina);
            }
        }
    }

    /**
     * Método que exclui uma disciplina e suas aulas
     *
     * @param disciplina recebida para identificar qual disciplina excluir
     * @return True caso foi excluida com sucesso. Caso contrário, retorna False.
     */
    public boolean excluirDisciplina(Disciplina disciplina) {
        for (Disciplina disciplinaExcluir : disciplinas) {
            if (disciplinaExcluir.equals(disciplina)) {
                disciplinas.remove(disciplinaExcluir);
                return true;
            }
        }
        return false;
    }
}
