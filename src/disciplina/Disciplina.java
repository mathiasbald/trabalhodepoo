/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package disciplina;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Débora Siqueira
 */
public class Disciplina implements Serializable {

    private String nomeDisciplina;
    private String nomeProfessor;
    private String curso;
    private int semestre;
    private int turma;
    private boolean notificar;
    private List<Aula> aulas = new ArrayList<>();

    public Disciplina(List<Aula> aulas, String nomeDisciplina, String nomeProfessor, String curso, int semestre, int turma, boolean notificar) {
        this.nomeDisciplina = nomeDisciplina;
        this.nomeProfessor = nomeProfessor;
        this.curso = curso;
        this.semestre = semestre;
        this.turma = turma;
        this.notificar = notificar;
        this.aulas = aulas;
    }

    /**
     * Método que verifica se existe uma aula no dia passado
     *
     * @param dia dia da semana que deseja verificar aula
     * @return True caso exist. Caso contrário, retorna False
     */
    public boolean contaisAulasNoDia(DiaDaSemana dia) {
        for (Aula aula : aulas) {
            if (aula.equalsDiaDaSemana(dia)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que adiciona aulas
     *
     * @param aula a ser adicionada
     */
    public void addAula(Aula aula) {
        aulas.add(aula);
    }

    /**
     * Método que retorna todas as aulas contidas em um dia da semana
     *
     * @param dia recebido para retornara as aulas contidas neste dia
     * @return List com todas as aulas contidas no dia passado
     */
    public List<Aula> getAulasDoDia(DiaDaSemana dia) {
        ArrayList<Aula> listaRetorno = new ArrayList<>();
        for (Aula aulasDoDia : aulas) {
            if (aulasDoDia.equalsDiaDaSemana(dia)) {
                listaRetorno.add(aulasDoDia);
            }
        }
        return listaRetorno;
    }

    public List<Aula> getTodasAulas() {
        return aulas;
    }

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public String getNomeProfessor() {
        return nomeProfessor;
    }

    public String getCurso() {
        return curso;
    }

    public int getSemestre() {
        return semestre;
    }

    public int getTurma() {
        return turma;
    }

    public boolean isNotificar() {
        return notificar;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public void setNomeProfessor(String nomeProfessor) {
        this.nomeProfessor = nomeProfessor;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public void setTurma(int turma) {
        this.turma = turma;
    }

    public void setNotificar(boolean notificar) {
        this.notificar = notificar;
    }

    public void setAulas(List<Aula> aulas) {
        this.aulas = aulas;
    }

    @Override
    public String toString() {
        String dados;
        dados = "Disciplina: " + this.nomeDisciplina + "\n" + "Nome do professor: " + this.nomeProfessor + "\n"
                + "Turma: " + this.turma + "\n" + "Curso: " + this.curso + "\n" + "Semestre: " + this.semestre;
        return dados;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     * Método equals para comparar se dois objetos (Disciplinas) são iguais
     *
     * @param obj recebido para ser comparado
     * @return True caso sejam iguais. Caso contrário, retorna false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Disciplina other = (Disciplina) obj;
        if (this.semestre != other.semestre) {
            return false;
        }
        if (this.turma != other.turma) {
            return false;
        }
        if (this.notificar != other.notificar) {
            return false;
        }
        if (!Objects.equals(this.nomeDisciplina, other.nomeDisciplina)) {
            return false;
        }
        if (!Objects.equals(this.nomeProfessor, other.nomeProfessor)) {
            return false;
        }
        if (!Objects.equals(this.curso, other.curso)) {
            return false;
        }
        if (!Objects.equals(this.aulas, other.aulas)) {
            return false;
        }
        return true;
    }

}
