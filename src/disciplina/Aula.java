/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package disciplina;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Débora Siqueira
 */
public class Aula implements Serializable {

    private DiaDaSemana diaSemana;
    private int sala;
    private Horario horarioInicio;
    private Horario horarioFinal;

    public Aula(DiaDaSemana diaSemana, int sala, Horario horarioInicio, Horario horarioFinal) {
        this.diaSemana = diaSemana;
        this.sala = sala;
        this.horarioInicio = horarioInicio;
        this.horarioFinal = horarioFinal;
    }

    /**
     * Método que verifica se dois dias são iguais
     *
     * @param dia recebido para comparar
     * @return True caso sejam iguais. Caso contrário, retorna False
     */
    public boolean equalsDiaDaSemana(DiaDaSemana dia) {
        return diaSemana.equals(dia);
    }

    public DiaDaSemana getDiaSemana() {
        return diaSemana;
    }

    public int getSala() {
        return sala;
    }

    public Horario getHorarioInicio() {
        return horarioInicio;
    }

    public Horario getHorarioFinal() {
        return horarioFinal;
    }

    public void setDiaSemana(DiaDaSemana diaSemana) {
        this.diaSemana = diaSemana;
    }

    public void setSala(int sala) {
        this.sala = sala;
    }

    public void setHorarioInicio(Horario horarioInicio) {
        this.horarioInicio = horarioInicio;
    }

    public void setHorarioFinal(Horario horarioFinal) {
        this.horarioFinal = horarioFinal;
    }

    @Override
    public String toString() {
        String dados;
        dados = "Horário de inicio: " + this.horarioInicio.toString() + "\n" + "Horário de término: " + this.horarioFinal.toString()
                + "\n" + "Sala: " + this.sala + "\n" + "Dia: " + this.diaSemana + "\n";
        return dados;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     * Método equals que verifica se dois objetos (aula) são iguais
     *
     * @param obj recebido para comparar
     * @return True caso sejam iguais. Caso contrário, retorna False
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aula other = (Aula) obj;
        if (this.sala != other.sala) {
            return false;
        }
        if (this.diaSemana != other.diaSemana) {
            return false;
        }
        if (!Objects.equals(this.horarioInicio, other.horarioInicio)) {
            return false;
        }
        if (!Objects.equals(this.horarioFinal, other.horarioFinal)) {
            return false;
        }
        return true;
    }

}
