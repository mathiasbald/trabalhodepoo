/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validacao;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Débora Siqueira
 */
public class Validacao {

    /**
     * Método que valida um texto apenas com letras
     *
     * @param texto recebido para validação
     * @return retorna a String passada caso ela contenha apenas letras
     */
    public static String nextLine(String texto) {
        Scanner entrada = new Scanner(System.in);
        boolean ficar;
        do {
            ficar = false;
            texto = entrada.nextLine();
            if (texto == null || texto.equals("") || texto.equals(" ")) {
                ficar = true;
                System.out.println("Formato de texto incorreto. Por favor, digite novamente:");
            } else {
                Pattern pattern = Pattern.compile("[0-9]");
                Matcher matcher = pattern.matcher(texto);
                if (matcher.find()) {
                    ficar = true;
                    System.out.println("Formato de texto incorreto. Por favor, Digite novamente: ");
                }
            }
        } while (ficar);
        return texto;
    }

    /**
     * Método que valida uma String que contenha apenas numeros
     *
     * @param numero recebido para ser validado
     * @return retorna a String passada caso ela contenha apenas numeros.
     */
    public static String nextInt(String numero) {
        Scanner e = new Scanner(System.in);
        boolean ficar;
        do {
            try {
                numero = e.nextLine();
                Integer.parseInt(numero);
                return numero;
            } catch (InputMismatchException | NumberFormatException e5) {
                ficar = true;
                System.out.println("Incorreto. Digite novamente.");
            }
        } while (ficar);
        return numero;
    }
}
