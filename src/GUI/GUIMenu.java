/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import validacao.Validacao;
import disciplina.Horario;
import disciplina.Disciplina;
import disciplina.Aula;
import disciplina.DiaDaSemana;
import dominio.GerenciadorDisciplina;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Matheus Montanha
 */
public class GUIMenu {

    GerenciadorDisciplina gerenciador = GerenciadorDisciplina.getInstance();

    public GUIMenu() {
    }

    /**
     * Método que mostra as opções disponíveis.
     *
     * @throws IOException exception de entrada e saida.
     */
    public void Menu() throws IOException {
        int opcao = Integer.MIN_VALUE;
        Scanner e = new Scanner(System.in);
        do {
            System.out.println("1 - Cadastre aula.");
            System.out.println("2 - Excluir aula. ");
            System.out.println("3 - Editar aula.");
            System.out.println("4 - Consultar aula.");
            System.out.println("0 - Sair.");

            System.out.println("O que deseja fazer?");
            try {
                opcao = e.nextInt();
            } catch (NumberFormatException e7) {
                System.out.println("Opção inválida.");
            }
            switch (opcao) {
                case 1:
                    cadastro();
                    break;
                case 2:
                    exclusao();
                    break;
                case 3:
                    editar();
                    break;
                case 4:
                    consultar();
                    break;
                case 5:
                    exclusao();
                case 0:
                    System.out.println("Até logo!");
                    break;
                default:
                    System.out.println("Opção inexistente. Escolha novamente.");
                    break;
            }
        } while (opcao != 0);
    }

    /**
     * Método que cadastra uma nova disciplina
     *
     * @throws IOException exception de entrada e saida caso ocorra
     */
    public void cadastro() throws IOException {
        List<Aula> aulas = new ArrayList<>();
        boolean ficar;
        Scanner e = new Scanner(System.in);
        String semestreTemp = null, turmaTemp = null, salaTemp = null;
        String nomeDisciplina = null, nomeProfessor = null, curso = null;
        int semestre, turma, sala;
        boolean notificar = false, ficarHorario;
        Horario inicioAula, terminoAula;
        System.out.println("Digite o nome da disciplina: ");
        nomeDisciplina = Validacao.nextLine(nomeDisciplina);
        System.out.println("Digite o nome do professor: ");
        nomeProfessor = Validacao.nextLine(nomeProfessor);
        System.out.println("Digite o nome do curso: ");
        curso = Validacao.nextLine(curso);
        System.out.println("Digite o semestre: ");
        semestreTemp = Validacao.nextInt(semestreTemp);
        semestre = Integer.parseInt(semestreTemp);
        System.out.println("Digite o número da sua turma: ");
        turmaTemp = Validacao.nextInt(turmaTemp);
        turma = Integer.parseInt(turmaTemp);
        System.out.println("Deseja receber notificações?\n"
                + "1 - Sim \n"
                + "2 - Não");
        String temp = null;
        int opcao;
        temp = Validacao.nextInt(temp);
        opcao = Integer.parseInt(temp);
        switch (opcao) {
            case 1:
                notificar = true;
                break;
            case 2:
                notificar = false;
                break;
            default:
                System.out.println("Opção inválida.");
                break;
        }
        boolean jaFoiSegunda = false, jaFoiTerça = false, jaFoiQuarta = false, jaFoiQuinta = false, jaFoiSexta = false, jaFoiSabado = false;
        do {
            ficar = true;
            System.out.println("Escolha os dias da semana que irá a ter disciplina: ");
            System.out.println("1 - Segunda-Feira.");
            System.out.println("2 - Terça-Feira.");
            System.out.println("3 - Quarta-Feira.");
            System.out.println("4 - Quinta-Feira.");
            System.out.println("5 - Sexta-Feira");
            System.out.println("6 - Sábado.");
            System.out.println("0 - Finalizar.");
            opcao = e.nextInt();
            switch (opcao) {
                case 0:
                    ficar = false;
                    break;
                case 1:
                    if (!jaFoiSegunda) {
                        DiaDaSemana segunda = DiaDaSemana.SEGUNDA;
                        System.out.println("Digite a sala:");
                        salaTemp = Validacao.nextInt(salaTemp);
                        sala = Integer.parseInt(salaTemp);
                        do {
                            ficarHorario = false;
                            System.out.println("Horario de inicio: ");
                            if ((inicioAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        do {
                            ficarHorario = false;
                            System.out.println("Horário de termino: ");
                            if ((terminoAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        Aula aulaSegunda = new Aula(segunda, sala, inicioAula, terminoAula);
                        jaFoiSegunda = true;
                        aulas.add(aulaSegunda);
                        break;
                    } else {
                        System.out.println("Você ja adicionou uma aula de segunda.");
                    }
                case 2:
                    if (!jaFoiTerça) {
                        DiaDaSemana terca = DiaDaSemana.TERCA;
                        System.out.println("Digite a sala:");
                        salaTemp = Validacao.nextInt(salaTemp);
                        sala = Integer.parseInt(salaTemp);
                        do {
                            ficarHorario = false;
                            System.out.println("Horario de inicio: ");
                            if ((inicioAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        do {
                            ficarHorario = false;
                            System.out.println("Horário de termino: ");
                            if ((terminoAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        Aula aulaTerca = new Aula(terca, sala, inicioAula, terminoAula);
                        jaFoiTerça = true;
                        aulas.add(aulaTerca);
                        break;
                    } else {
                        System.out.println("Você já adicinou uma aula de terça.");
                    }
                case 3:
                    if (!jaFoiQuarta) {
                        DiaDaSemana quarta = DiaDaSemana.QUARTA;
                        System.out.println("Digite a sala:");
                        salaTemp = Validacao.nextInt(salaTemp);
                        sala = Integer.parseInt(salaTemp);
                        do {
                            ficarHorario = false;
                            System.out.println("Horario de inicio: ");
                            if ((inicioAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        do {
                            ficarHorario = false;
                            System.out.println("Horário de termino: ");
                            if ((terminoAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        Aula aulaQuarta = new Aula(quarta, sala, inicioAula, terminoAula);
                        jaFoiQuarta = true;
                        aulas.add(aulaQuarta);
                        break;
                    } else {
                        System.out.println("Você já adicinou uma aula de quarta.");
                    }
                case 4:
                    if (!jaFoiQuinta) {
                        DiaDaSemana quinta = DiaDaSemana.QUINTA;
                        System.out.println("Digite a sala:");
                        salaTemp = Validacao.nextInt(salaTemp);
                        sala = Integer.parseInt(salaTemp);
                        do {
                            ficarHorario = false;
                            System.out.println("Horario de inicio: ");
                            if ((inicioAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        do {
                            ficarHorario = false;
                            System.out.println("Horário de termino: ");
                            if ((terminoAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        Aula aulaQuinta = new Aula(quinta, sala, inicioAula, terminoAula);
                        jaFoiQuinta = true;
                        aulas.add(aulaQuinta);
                        break;
                    } else {
                        System.out.println("Você já adicinou uma aula de quinta.");
                    }
                case 5:
                    if (!jaFoiSexta) {
                        DiaDaSemana sexta = DiaDaSemana.SEXTA;
                        System.out.println("Digite a sala:");
                        salaTemp = Validacao.nextInt(salaTemp);
                        sala = Integer.parseInt(salaTemp);
                        do {
                            ficarHorario = false;
                            System.out.println("Horario de inicio: ");
                            if ((inicioAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        do {
                            ficarHorario = false;
                            System.out.println("Horário de termino: ");
                            if ((terminoAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        Aula aulaSexta = new Aula(sexta, sala, inicioAula, terminoAula);
                        jaFoiSexta = true;
                        aulas.add(aulaSexta);
                        break;
                    } else {
                        System.out.println("Você já adicinou uma aula de terça.");
                    }
                case 6:
                    if (!jaFoiSabado) {
                        DiaDaSemana sabado = DiaDaSemana.SABADO;
                        System.out.println("Digite a sala:");
                        salaTemp = Validacao.nextInt(salaTemp);
                        sala = Integer.parseInt(salaTemp);
                        do {
                            ficarHorario = false;
                            System.out.println("Horario de inicio: ");
                            if ((inicioAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        do {
                            ficarHorario = false;
                            System.out.println("Horário de termino: ");
                            if ((terminoAula = Horario.parseHorario(e.next())) == null) {
                                System.out.println("Horário inválido. Digite novamente: ");
                                ficarHorario = true;
                            }
                        } while (ficarHorario);
                        Aula aulaSabado = new Aula(sabado, sala, inicioAula, terminoAula);
                        jaFoiSabado = true;
                        aulas.add(aulaSabado);
                        break;
                    } else {
                        System.out.println("Você já adicinou uma aula de terça.");
                    }
                    break;
                default:
                    System.out.println("Dia inválido.");
                    break;
            }
        } while (ficar);
        if (aulas.size() > 0) {
            Disciplina disciplina = new Disciplina(aulas, nomeDisciplina, nomeProfessor, curso, semestre, turma, notificar);
            gerenciador.addDisciplina(disciplina);
            gerenciador.salvar();
            System.out.println("REALIZADO COM SUCESSO!");
        } else {
            System.out.println("Selecione ao menos uma disciplina");
        }
    }

    /**
     * Método que edita uma disciplina
     *
     * @throws IOException exception de entrada e saida caso ocorra
     */
    public void editar() throws IOException {
        Scanner e = new Scanner(System.in);
        List<Aula> aulas = new ArrayList<>();
        List<Disciplina> listaRecebeEditar;
        boolean ficar = false;
        String semestreTemp = null, turmaTemp = null, salaTemp = null;
        int opcao, opcao2, semestre, turma, sala;
        DiaDaSemana diaRecebe = null;
        String nomeDisciplina = null, nomeProfessor = null, curso = null;
        boolean notificar = false;
        Horario inicioAula, terminoAula;
        do {
            System.out.println("Selecione o dia da semana da aula que deseja editar: ");
            System.out.println("1 - Segunda-Feira.");
            System.out.println("2 - Terça-Feira.");
            System.out.println("3 - Quarta-Feira.");
            System.out.println("4 - Quinta-Feira.");
            System.out.println("5 - Sexta-Feira");
            System.out.println("6 - Sábado.");
            System.out.println("0 - Finalizar.");
            opcao = e.nextInt();
            switch (opcao) {
                case 1:
                    diaRecebe = DiaDaSemana.SEGUNDA;
                    break;
                case 2:
                    diaRecebe = DiaDaSemana.TERCA;
                    break;
                case 3:
                    diaRecebe = DiaDaSemana.QUARTA;
                    break;
                case 4:
                    diaRecebe = DiaDaSemana.QUINTA;
                    break;
                case 5:
                    diaRecebe = DiaDaSemana.SEXTA;
                    break;
                case 6:
                    diaRecebe = DiaDaSemana.SABADO;
                    break;
                case 0:
                    break;
                default:
                    ficar = true;
                    System.out.println("Opção inexistente. Tente novamente");
                    break;
            }
        } while (ficar);
        listaRecebeEditar = gerenciador.getDisciplinasDoDia(diaRecebe);
        if (!listaRecebeEditar.isEmpty()) {
            int contador = 0;
            for (Disciplina disciplina : listaRecebeEditar) {
                System.out.println("ID: " + contador + "\n" + disciplina.toString());
                for (Aula aula : disciplina.getAulasDoDia(diaRecebe)) {
                    System.out.println(aula.toString());
                }
                contador++;
            }
            System.out.println("ESSAS SÃO AS AULAS QUE CONTÉM NA " + diaRecebe + ".");
            System.out.println("Idêntifique o ID da aula que deseja editar e identifique-o: ");
            String IDTemp = null;
            IDTemp = Validacao.nextInt(IDTemp);
            opcao2 = Integer.parseInt(IDTemp);
            if (opcao2 <= listaRecebeEditar.size() - 1) {
                Disciplina disciplinaEditar = listaRecebeEditar.get(opcao2);
                System.out.println("Digite o nome da disciplina: ");
                nomeDisciplina = Validacao.nextLine(nomeDisciplina);
                System.out.println("Digite o nome do professor: ");
                nomeProfessor = Validacao.nextLine(nomeProfessor);
                System.out.println("Digite o nome do curso: ");
                curso = Validacao.nextLine(curso);
                System.out.println("Digite o semestre: ");
                semestreTemp = Validacao.nextInt(semestreTemp);
                semestre = Integer.parseInt(semestreTemp);
                System.out.println("Digite o número da sua turma: ");
                turmaTemp = Validacao.nextInt(turmaTemp);
                turma = Integer.parseInt(turmaTemp);
                System.out.println("Deseja receber notificações?\n"
                        + "1 - Sim \n"
                        + "2 - Não");
                int opcao3;
                String temp = null;
                temp = Validacao.nextInt(temp);
                opcao3 = Integer.parseInt(temp);
                switch (opcao3) {
                    case 1:
                        notificar = true;
                        break;
                    case 2:
                        notificar = false;
                        break;
                    default:
                        System.out.println("Opção inválida.");
                        break;
                }
                boolean jaFoiSegunda = false, jaFoiTerça = false, jaFoiQuarta = false, jaFoiQuinta = false, jaFoiSexta = false, jaFoiSabado = false;
                do {
                    ficar = true;
                    System.out.println("Escolha os dias da semana que irá a ter disciplina: ");
                    System.out.println("1 - Segunda-Feira.");
                    System.out.println("2 - Terça-Feira.");
                    System.out.println("3 - Quarta-Feira.");
                    System.out.println("4 - Quinta-Feira.");
                    System.out.println("5 - Sexta-Feira");
                    System.out.println("6 - Sábado.");
                    System.out.println("0 - Finalizar.");
                    opcao = e.nextInt();
                    switch (opcao) {
                        case 0:
                            ficar = false;
                            break;
                        case 1:
                            if (!jaFoiSegunda) {
                                DiaDaSemana segunda = DiaDaSemana.SEGUNDA;
                                System.out.println("Digite a nova sala:");
                                salaTemp = Validacao.nextInt(salaTemp);
                                sala = Integer.parseInt(salaTemp);
                                System.out.println("Novo horario de inicio: ");
                                inicioAula = Horario.parseHorario(e.next());
                                System.out.println("Novo horário de termino: ");
                                terminoAula = Horario.parseHorario(e.next());
                                Aula aulaSegunda = new Aula(segunda, sala, inicioAula, terminoAula);
                                jaFoiSegunda = true;
                                aulas.add(aulaSegunda);
                                break;
                            } else {
                                System.out.println("Você ja adicionou uma aula de segunda.");
                            }
                        case 2:
                            if (!jaFoiTerça) {
                                DiaDaSemana terca = DiaDaSemana.TERCA;
                                System.out.println("Digite a nova sala:");
                                salaTemp = Validacao.nextInt(salaTemp);
                                sala = Integer.parseInt(salaTemp);
                                System.out.println("Novo horario de inicio: ");
                                inicioAula = Horario.parseHorario(e.next());
                                System.out.println("Novo horário de termino: ");
                                terminoAula = Horario.parseHorario(e.next());
                                Aula aulaTerca = new Aula(terca, sala, inicioAula, terminoAula);
                                jaFoiTerça = true;
                                aulas.add(aulaTerca);
                                break;
                            } else {
                                System.out.println("Você já adicinou uma aula de terça.");
                            }
                        case 3:
                            if (!jaFoiQuarta) {
                                DiaDaSemana quarta = DiaDaSemana.QUARTA;
                                System.out.println("Digite a nova sala:");
                                salaTemp = Validacao.nextInt(salaTemp);
                                sala = Integer.parseInt(salaTemp);
                                System.out.println("Novo horario de inicio: ");
                                inicioAula = Horario.parseHorario(e.next());
                                System.out.println("Novo horário de termino: ");
                                terminoAula = Horario.parseHorario(e.next());
                                Aula aulaQuarta = new Aula(quarta, sala, inicioAula, terminoAula);
                                jaFoiQuarta = true;
                                aulas.add(aulaQuarta);
                                break;
                            } else {
                                System.out.println("Você já adicinou uma aula de quarta.");
                            }
                        case 4:
                            if (!jaFoiQuinta) {
                                DiaDaSemana quinta = DiaDaSemana.QUINTA;
                                System.out.println("Digite a nova sala:");
                                salaTemp = Validacao.nextInt(salaTemp);
                                sala = Integer.parseInt(salaTemp);
                                System.out.println("Novo horario de inicio: ");
                                inicioAula = Horario.parseHorario(e.next());
                                System.out.println("Novo horário de termino: ");
                                terminoAula = Horario.parseHorario(e.next());
                                Aula aulaQuinta = new Aula(quinta, sala, inicioAula, terminoAula);
                                jaFoiQuinta = true;
                                aulas.add(aulaQuinta);
                                break;
                            } else {
                                System.out.println("Você já adicinou uma aula de quinta.");
                            }
                        case 5:
                            if (!jaFoiSexta) {
                                DiaDaSemana sexta = DiaDaSemana.SEXTA;
                                System.out.println("Digite a nova sala:");
                                salaTemp = Validacao.nextInt(salaTemp);
                                sala = Integer.parseInt(salaTemp);
                                System.out.println("Novo horario de inicio: ");
                                inicioAula = Horario.parseHorario(e.next());
                                System.out.println("Novo horário de termino: ");
                                terminoAula = Horario.parseHorario(e.next());
                                Aula aulaSexta = new Aula(sexta, sala, inicioAula, terminoAula);
                                jaFoiSexta = true;
                                aulas.add(aulaSexta);
                                break;
                            } else {
                                System.out.println("Você já adicinou uma aula de terça.");
                            }
                        case 6:
                            if (!jaFoiSabado) {
                                DiaDaSemana sabado = DiaDaSemana.SABADO;
                                System.out.println("Digite a nova sala:");
                                salaTemp = Validacao.nextInt(salaTemp);
                                sala = Integer.parseInt(salaTemp);
                                System.out.println("Novo horario de inicio: ");
                                inicioAula = Horario.parseHorario(e.next());
                                System.out.println("Novo horário de termino: ");
                                terminoAula = Horario.parseHorario(e.next());
                                Aula aulaSabado = new Aula(sabado, sala, inicioAula, terminoAula);
                                jaFoiSabado = true;
                                aulas.add(aulaSabado);
                                break;
                            } else {
                                System.out.println("Você já adicinou uma aula de terça.");
                            }
                            break;
                        default:
                            System.out.println("Dia inválido.");
                            break;
                    }
                } while (ficar);
                Disciplina disciplina = new Disciplina(aulas, nomeDisciplina, nomeProfessor, curso, semestre, turma, notificar);
                gerenciador.replaceDisciplina(disciplinaEditar, disciplina);
                System.out.println("Editado com sucesso.");
                gerenciador.salvar();
            } else {
                System.out.println("ID passado inexistente.");
            }
        } else {
            System.out.println("Não contém aulas cadastradas na " + diaRecebe);
        }
    }

    /**
     * Método que consulta as aulas cadastradas
     */
    public void consultar() {
        Scanner e = new Scanner(System.in);
        int opcao;
        String temp = null;
        DiaDaSemana diaRecebe = null;
        List<Disciplina> recebe;
        System.out.println("Seleciona o dia que deseja consultar suas aulas:");
        System.out.println("1 - Segunda-Feira.");
        System.out.println("2 - Terça-Feira.");
        System.out.println("3 - Quarta-Feira.");
        System.out.println("4 - Quinta-Feira.");
        System.out.println("5 - Sexta-Feira");
        System.out.println("6 - Sábado.");
        System.out.println("0 - Sair");
        temp = Validacao.nextInt(temp);
        opcao = Integer.parseInt(temp);
        switch (opcao) {
            case 1:
                diaRecebe = DiaDaSemana.SEGUNDA;
                break;
            case 2:
                diaRecebe = DiaDaSemana.TERCA;
                break;
            case 3:
                diaRecebe = DiaDaSemana.QUARTA;
                break;
            case 4:
                diaRecebe = DiaDaSemana.QUINTA;
                break;
            case 5:
                diaRecebe = DiaDaSemana.SEXTA;
                break;
            case 6:
                diaRecebe = DiaDaSemana.SABADO;
                break;
            case 0:
                break;
        }
        recebe = gerenciador.getDisciplinasDoDia(diaRecebe);
        if (!recebe.isEmpty()) {
            for (Disciplina disciplina : recebe) {
                System.out.println(disciplina.toString());
                for (Aula aula : disciplina.getAulasDoDia(diaRecebe)) {
                    System.out.println(aula.toString());
                }
            }
        } else {
            System.out.println("Não contém aulas neste dia.");
        }
    }

    /**
     * Método que exclui uma aula ou disciplina
     *
     * @throws IOException exception de entrada e saida caso ocorra
     */
    public void exclusao() throws IOException {
        List<Disciplina> listaPegaRetorno;
        DiaDaSemana diaRecebe = null;
        String temp = null;
        int opcao;
        System.out.println("Seleciona o dia da aula que deseja remover:");
        System.out.println("1 - Segunda-Feira.");
        System.out.println("2 - Terça-Feira.");
        System.out.println("3 - Quarta-Feira.");
        System.out.println("4 - Quinta-Feira.");
        System.out.println("5 - Sexta-Feira");
        System.out.println("6 - Sábado.");
        System.out.println("0 - Sair");
        temp = Validacao.nextInt(temp);
        opcao = Integer.parseInt(temp);
        switch (opcao) {
            case 1:
                diaRecebe = DiaDaSemana.SEGUNDA;
                break;
            case 2:
                diaRecebe = DiaDaSemana.TERCA;
                break;
            case 3:
                diaRecebe = DiaDaSemana.QUARTA;
                break;
            case 4:
                diaRecebe = DiaDaSemana.QUINTA;
                break;
            case 5:
                diaRecebe = DiaDaSemana.SEXTA;
                break;
            case 6:
                diaRecebe = DiaDaSemana.SABADO;
                break;
            case 0:
                break;
        }
        listaPegaRetorno = gerenciador.getDisciplinasDoDia(diaRecebe);
        int contador = 0;
        for (Disciplina disciplina : listaPegaRetorno) {
            System.out.println("ID: " + contador + "\n" + disciplina.toString());
            for (Aula aula : disciplina.getAulasDoDia(diaRecebe)) {
                System.out.println(aula.toString());
            }
            contador++;
        }
        System.out.println("ESSAS SÃO AS AULAS QUE CONTÉM NA " + diaRecebe + ".");
        System.out.println("Idêntifique o ID da aula que deseja remover e identifique-o: ");
        String IDTemp = null;
        IDTemp = Validacao.nextInt(IDTemp);
        opcao = Integer.parseInt(IDTemp);
        System.out.println("1 - Remover uma aula");
        System.out.println("2 - Remover todas as aulas");
        int opcaoRemover;
        IDTemp = Validacao.nextInt(IDTemp);
        opcaoRemover = Integer.parseInt(IDTemp);
        switch (opcaoRemover) {
            case 1:
                if (opcao <= listaPegaRetorno.size() - 1) {
                    Disciplina disciplinaRemover = listaPegaRetorno.get(opcao);
                    if (gerenciador.deleteAulaDoDia(disciplinaRemover, diaRecebe)) {
                        System.out.println("Aula removida com sucesso.");
                        gerenciador.salvar();
                    }
                } else {
                    System.out.println("ID desconhecido.");
                }
                break;
            case 2:
                if (opcao <= listaPegaRetorno.size() - 1) {
                    Disciplina disciplinaRemover = listaPegaRetorno.get(opcao);
                    if (gerenciador.excluirDisciplina(disciplinaRemover)) {
                        System.out.println("Aulas removida com sucesso.");
                        gerenciador.salvar();
                    } else {
                        System.out.println("Não foi possível remover a aula desejada.");
                    }
                } else {
                    System.out.println("ID desconhecido.");
                }
                break;
            default:
                System.out.println("Opção escolhida inválida.");
        }
    }
}
